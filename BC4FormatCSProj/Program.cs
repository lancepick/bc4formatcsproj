﻿using System.Linq;
using System.Xml.Linq;

namespace BC4FormatCSProj
{
    class Program
    {
        static void Main(string[] args)
        {
            //args[0] - Input File
            //args[1] - Save Path
            SortAndSave(args[0], args[1]);
        }

        public static void SortAndSave(string inputFile, string savepath)
        {
            XNamespace ns = "http://schemas.microsoft.com/developer/msbuild/2003";
            XDocument doc = XDocument.Load(inputFile);
            doc.Root.Elements(ns + "ItemGroup").ToList()
                .ForEach(elem =>
                    elem.ReplaceNodes(
                        elem.Elements()
                        .OrderBy(o => o.Name.LocalName)
                        .ThenBy(o => o.Attribute("Include")?.Value)
                    )
                );
            doc.Save(savepath);
        }
    }
}
